

var app = new Vue({
    el: '#app',
    created(){
        this.GetUsers()
    },
    data: () => ({
        title: 'MAVEN',
        users: [],
        user: {
            id: null,
            name:'',
            email:'',
            phone:'',
        }
    }),
    methods:{
        SaveUser(){
            if(this.user.id){
                axios.put(`/api/users/${this.user.id}`, this.user).then(() => {
                    this.GetUsers()
                })
            }else{
                axios.post('/api/users', this.user).then(() => {
                    this.GetUsers()
                })
            }
        },
        GetUsers(){
            axios.get('/api/users').then(res => {
                this.users = res.data
                this.ClearUser()
            })
        },
        DeleteUser(id){
            if(id && confirm('Are you user you want to delete this user?')){
                axios.delete(`/api/users/${id}`).then(() => {
                    this.GetUsers()
                })
            }
        },
        SelectUser(id){
            if(id){
                axios.get(`/api/users/${id}`).then(res => {
                    this.user = res.data
                })
            }
        },
        ClearUser(){
            this.user = {
                id: null,
                name: '',
                email: '',
                phone: '',
            }
        },
    }
})