const express = require('express');
const router = express.Router();
const _ = require('lodash')

var data = require('../../services/data')
function genId() {
    var highestId = _.reduce(data.users, (prev, curr) => {
        console.log(prev, curr)

        if ( !prev || parseInt(curr.id) > parseInt(prev.id) ) {
            return curr
        } else {
            return prev
        }
    })
    return parseInt(highestId.id) + 1
}
router.get('/', (req, res) => {
    res.json(data.users)
})

router.get('/:id', (req, res) => {
    var user = _.find(data.users, o => {
        return o.id == req.params.id
    })
    res.json(user)
})

router.post('/', (req, res) => {
    var newUser = _.pick(req.body, ['name','email','phone'])
    newUser.id = genId()
    data.users.push(newUser)
    res.json(newUser)
})

router.put('/:id', (req, res) => {
    var index = _.findIndex(data.users, o => {
        return o.id = req.params.id
    } )
    valideData = _.pick(req.body, ['name', 'email', 'phone'])
    valideData.id = data.users[index].id
    data.users[index] = valideData
    res.json(valideData)
})

router.delete('/:id', (req, res) => {
    removed_user = _.remove(data.users, o => {
        return o.id == req.params.id
    })
    res.json(removed_user)
})

module.exports = router;