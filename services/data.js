var users = [
    { id: 1, name: 'Brandon Crowe', email: 'bcrowe306@gmail.com', phone: '8136062719' },
    { id: 2, name: 'Jazmin Crowe', email: 'jcrowe521@gmail.com', phone: '8133732457' },
    { id: 3, name: 'Illiana Crowe', email: 'icrowe@gmail.com', phone: '8136062719' },
    { id: 4, name: 'Camden Crowe', email: 'camcrowe@gmail.com', phone: '8136062719' },
    { id: 5, name: 'Bryson Crowe', email: 'brycrowe@gmail.com', phone: '8136062719' },
    { id: 6, name: 'Annalise Crowe', email: 'acrowe@gmail.com', phone: '8136062719' },
]
module.exports = {
    users
}